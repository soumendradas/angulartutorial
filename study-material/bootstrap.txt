bootstrap
bootstrap is an open-source repository that provides a couple of native Angular widgets which are built
using Bootstrap 4 CSS. As you know Bootstrap is one of the most popular CS frameworks which is
used to build stylish and modern applications, which has HTML, CSS and JavaScript libraries. In order
to use Bootstrap frameworks in the Angular app, earlier we would have had to use bootstrap CsS as
well as its JavaScript files, but "ng-bootstrap" gives us the best approach to use without any JS file in
our Angular applications.


No 3rd Party libraries required
Since “bootstrap" itself a module that encapsulates all of the Bootstrap
functionality so there is no need to use any other 3rd party library into our Angular
app. As a result, there is no dependency even on jQuery or on Bootstrap's
JavaScript library. Only dependencies are
Angular 4 and above
Bootstrap 4.0.0 (CSS file only, Bootstrap.js and Bootstrap.min.js are not required)


Browser Support with “ng-bootstrap" module
It supports all of the browsers that has the same intersection of browsers supported by
both Angular 4+ and Bootstrap 4.0.0 CSS only like,
* Chrome (latest)
Firefox (latest)
* Edge (13+)
* IE (10+)
iOS (7+)
Android (5.0 +)
Safari (7+) and newer etc.


*Installing bootstrap
Now we will see “How to install ng-bootstrap module to be used in Angular apps?"
so that we could use the bootstrap components provided by “ng-bootstrap".
Since we have Angular 5 app created by Angular CLI and using code editor as VS
code, we will setup ng-bootstrap in the same application.
*There are two way to use “ng-bootstrap" in an Angular app,
First, download Bootstrap CSS library from its official website and copy the
bootstrap.min.css file and paste in into your Angular app (under src) 1
it in styles array of Angular-cli.json of app like :

Second, install the Bootstrap module using npm command. Open VS code terminal
window or you can use windows command prompt and type below command and
install,
npm install bootstrap --save
To verify if the ng-bootstrap module is installed on your machine, go to
package.json file of your app and check Bootstrap module .
 



