import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-util',
  templateUrl: './util.component.html',
  styleUrls: ['./util.component.css']
})
export class UtilComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  title:string = "Demo Binding";
  cols:number = 3
  bdr:number = 1

}
