import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoopsComponent } from './loops/loops.component';

const routes: Routes = [
  {path:"loops", component:LoopsComponent},
  {path:"employee", 
  loadChildren: ()=> import('./employee/employee.module')
        .then(m=>m.EmployeeModule)}  //For lazy loading
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
