import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditComponent } from './edit/edit.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {
    path: "", 
    children: [
      {path:"register", component: RegisterComponent},
      {path:"edit", component: EditComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
