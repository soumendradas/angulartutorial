import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe',
  templateUrl: './pipe.component.html',
  styleUrls: ['./pipe.component.css']
})
export class PipeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  title:string = "Pipe Demo";

  dob:Date = new Date('1995-06-23')

  salary:number = 50000

  students:any[]=[{
    id:1, name: "soumendra", gender: "male"
  },{
    id:2, name:"Ankita", gender:"female"
  },{
    id:3, name: "SD", gender: "male"
  },{
    id:4, name: "Soni", gender: "female"
  },{
    id:5, name: "Lila", gender: "female"
  },{
    id:6, name: "Bapunu", gender: "male"
  }]

}
