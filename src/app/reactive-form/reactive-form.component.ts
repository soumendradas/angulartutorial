import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms'
import { User } from '../user';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  signupForm: FormGroup;
  contactForm: any;
  firstName:string = "Hello";
  lastName:string = "";
  email:string = "";
  password:string = "";

  user:User = new User();

  constructor(private formBuilder: FormBuilder) {
    this.signupForm = formBuilder.group({
      fName:['', Validators.compose([Validators.required, Validators.maxLength(12)])],
      lName: ['', Validators.compose([Validators.required, Validators.maxLength(12), Validators.pattern('[A-Za-z]{1,12}')])],
      emailId: ['', Validators.compose([Validators.required, Validators.maxLength(12), Validators.email])],
      userPassword: new FormControl(),
      contactNos: formBuilder.array([
        new FormControl(9658441281),
        new FormControl(7008261517)
      ]),
      language: formBuilder.array([])
      
    })
    this.addLanguageFields();
   }

  ngOnInit(): void {
    // this.signupForm.valueChanges.subscribe((data:User)=>{
    //   this.user = data;
    // })

    // this.signupForm.statusChanges.subscribe(data=>{
    //   console.log(data);
    // })

    // const arr = new FormArray([
    //   new FormControl("Bapunu"),
    //   new FormControl("Das")
    // ]);

    // console.log(arr.value);
    // console.log(arr.status)

    this.contactForm = new FormGroup({
      contactNos: new FormArray([
        new FormControl(9658441281),
        new FormControl(7008261517)
      ])
    })

  }

  getContactNos(){
    return (<FormArray>this.signupForm.controls.contactNos).controls;
  }

  getLanguages() {
    return (<FormArray>this.signupForm.controls.language).controls;
  }

  addLanguageFields(){
    let lanGroup = this.formBuilder.group({
      lang:new FormControl("")
    });

    (<FormArray>this.signupForm.controls.language).push(lanGroup);
    console.log(this.signupForm.value);
  }

  addLang(){
    this.addLanguageFields();
  }

  postData(signupForm:any){
    // this.firstName = signupForm.get('fName').value;
    // console.log(this.firstName);
    // signupForm.reset();
    this.user = signupForm.value;
    console.log(this.user);
  }

  fillData(){
    this.signupForm.setValue({
      "fName":"Bapunu",
      "lName":"Das",
      "emailId":"soumendra.95@gmail.com",
      "userPassword":"Bapunu"
    });
  }

  contactNos():FormArray{
    return this.signupForm.get('contactNos') as FormArray;
  }

  saveNumber(contactForm:any){
    console.log(contactForm.get('contactNos').value);
  }

}

