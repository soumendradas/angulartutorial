import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  registration(regForm:any){
    console.log(regForm);

    var firstName = regForm.controls.firstName.value;
    var lastName = regForm.controls.lastName.value

    console.log(firstName);
    console.log(lastName);
  }

}
