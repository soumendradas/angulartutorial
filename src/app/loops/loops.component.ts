import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loops',
  templateUrl: './loops.component.html',
  styleUrls: ['./loops.component.css']
})
export class LoopsComponent implements OnInit {

  students:any = [];
  constructor() {
    this.students =[
      {id:1, 'name':'Soumendra'},
      {id:2, 'name':'Soumendra'},
      {id:3, 'name':'Soumendra'},
      {id:4, 'name':'Soumendra'},
      {id:5, 'name':'Soumendra'},
      {id:6, 'name':'Soumendra'},
      {id:7, 'name':'Bapunu'},
    ]
   }

  
  ngOnInit(): void {
    
  }

  getMoreStudent(){
    this.students =[
      {id:1, 'name':'Soumendra'},
      {id:2, 'name':'Soumendra'},
      {id:3, 'name':'Soumendra'},
      {id:4, 'name':'Soumendra'},
      {id:5, 'name':'Soumendra'},
      {id:6, 'name':'Soumendra'},
      {id:7, 'name':'Bapunu'},
      {id:8, 'name':'sd'},
      {id:9, 'name':'bao'},
      {id:10, 'name':'bsasj'},
      {id:11, 'name':'sdjkhksj'},
    ]
  }

  trackStudentId(index:number, student:any):string{
    return student.id;
  }

}
