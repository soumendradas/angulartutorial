import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import { CompanyModule } from './company/company.module'; 


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoopsComponent } from './loops/loops.component';
import { UtilComponent } from './util/util.component';
import { PipeComponent } from './pipe/pipe.component';
import { MypipePipe } from './mypipe.pipe';
import { TemplateFormComponent } from './template-form/template-form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { UserModule } from './user/user.module';
//import { EmployeeModule } from './employee/employee.module';

@NgModule({
  declarations: [
    AppComponent,
    LoopsComponent,
    UtilComponent,
    PipeComponent,
    MypipePipe,
    TemplateFormComponent,
    ReactiveFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    CompanyModule,
    UserModule,
    //EmployeeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
